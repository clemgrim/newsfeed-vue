import { addDecorator, configure } from '@storybook/vue';
import { withOptions } from '@storybook/addon-options';

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import 'bootstrap/scss/bootstrap.scss';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);

const storybookOptions = {
  showAddonPanel: false,
  name: 'Hosco',
  url: 'https://gitlab.com/hosco/hosco-v2',
};

addDecorator(withOptions(storybookOptions));

const req = require.context('../src/components', true, /.stories.js$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
