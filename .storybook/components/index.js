import StoryContainer from './StoryContainer.vue';
import StoryPage from './StoryPage.vue';

export { StoryContainer, StoryPage };
