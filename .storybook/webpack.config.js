const config = require('@vue/cli-service/webpack.config.js');
const path = require('path');

module.exports = (baseConfig, env, defaultConfig) => {
  defaultConfig.module.rules = config.module.rules;

  // we don't take the whole resolve config from vue because it will load vue runtime version
  // and we need the compiler with storybook
  defaultConfig.resolve.alias = {
    '@': path.resolve(__dirname, '../../src'),
    '~storybook': path.resolve(__dirname, './'),
    vue$: 'vue/dist/vue.esm.js'
  };

  return defaultConfig;
};

