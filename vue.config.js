const StyleLintPlugin = require('stylelint-webpack-plugin');
const path = require('path');

module.exports = {
  configureWebpack: {
    module: {
      rules: [
        {
          include: path.resolve('node_modules', 'bootstrap-vue'),
          sideEffects: false,
        }
      ]
    },
    resolve: {
      alias: {
        '~storybook': path.resolve(__dirname, './.storybook'),
      }
    },
    plugins: [
      new StyleLintPlugin({
        files: ['src/**/*.{vue,css,scss}'],
        syntax: 'scss'
      })
    ]
  },
  css: {
    loaderOptions: {
      css: {
        localIdentName: '[local]_[hash:base62:4]',
        camelCase: 'only'
      }
    }
  }
};
