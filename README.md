# newsfeed-vue

This project was created with Vue CLI 3.

It uses:
- Webpack (babel / node sass)
- VueJS
- Vuex
- Vue Router
- Storybook
- Lint tools (eslint, stylelint, commitlint)

## TODO
- More Unit tests
- E2E tests
- Finish newsfeed

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Storybook
```
yarn serve:storybook
```
