export default [
  {
    link: 'https://www.hosco.com/en/job/msc-cruises/itv-coordinator',
    title: 'ITV Coordinator',
    picture: 'https://www.hosco.com/image/logo/553949/200/200',
    info: 'MSC Cruises'
  },
  {
    link: 'https://www.hosco.com/en/job/hôtel-de-la-paix/it-engineer-informaticien-technicien',
    title: 'IT / Engineer - Informaticien / Technicien',
    picture: 'https://www.hosco.com/image/logo/1400696/200/200',
    info: 'The Ritz-Carlton Hotel de la Paix, Geneva'
  },
  {
    link: 'https://www.hosco.com/en/job/marriott-paris-cluster-office/multi-property-system-manager-paris-h-f',
    title: 'Multi-Property System Manager - Paris H/F',
    picture: 'https://www.hosco.com/image/logo/771224/200/200',
    info: 'Marriott Paris Cluster Office'
  },
  {
    link: 'https://www.hosco.com/en/job/sixt/internal-business-manager',
    title: 'Internal Business Manager',
    picture: 'https://www.hosco.com/image/logo/478375/200/200',
    info: 'Sixt'
  },
  {
    link: 'https://www.hosco.com/en/job/sixt/senior-sea-manager-for-us-market-m-f',
    title: 'Senior SEA Manager for US Market (m/f)',
    picture: 'https://www.hosco.com/image/logo/478375/200/200',
    info: 'Sixt'
  }
];
