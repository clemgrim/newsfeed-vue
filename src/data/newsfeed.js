export default [
  {
    'comments':[

    ],
    'is_administrable':false,
    'is_mine':false,
    'is_liked':false,
    'share_count':0,
    'like_count':3,
    'comment_count':0,
    'url':'https://www.hosco.com/fr/newsfeed/status/885426',
    'story':'a partag\u00e9 une photo',
    'content':'WWW.CIPAS.INFO',
    'is_available':true,
    'id':885426,
    'owner':{
      'avatar':'https://www.hosco.com/image/logo/1290546/200/200',
      'id':241005,
      'creation_date':'2016-12-22T13:39:47+0100',
      'update_date':'2018-06-27T22:03:43+0200',
      'slug':'giancarlo-pastore',
      'location':{
        'address_display':'Pi\u00e9mont, Italie'
      },
      'name':'GIANCARLO PASTORE',
      'type':'member'
    },
    'mentions':[

    ],
    'creation_date':'2018-08-11T07:44:28+0200',
    'update_date':'2018-08-11T07:44:40+0200',
    'thumbs':[
      {
        'path':'https://www.hosco.com/image/news-picture/1507978',
        'width':600,
        'height':425.9354177344951,
        'type':'image',
        'extension':'jpeg'
      }
    ],
    'files':[
      {
        'path':'https://cdn.hosco.com/upload/7c5e751870ed9f7e89a1cadc2896d8fdb8152bc05b6e77bc4a1370.24432144.jpeg',
        'title':'ON LINE MASTER HOTEL \u0026 REVENUE MANAGEMENT.jpeg',
        'extension':'jpeg',
        'creation_date':'2018-08-11T07:44:28+0200',
        'width':1951,
        'height':1385,
        'type':'image'
      }
    ],
    'type':'picture'
  },
  {
    'comments':[

    ],
    'is_administrable':false,
    'is_mine':false,
    'is_liked':false,
    'share_count':0,
    'like_count':0,
    'comment_count':0,
    'url':'https://www.hosco.com/fr/newsfeed/status/885244',
    'story':'a partag\u00e9 une photo',
    'content':'Ready for news adventures @BrennersParkHotel \n#wherethemagichappens #newdestinations',
    'is_available':true,
    'id':885244,
    'owner':{
      'avatar':'https://www.hosco.com/image/logo/753068/200/200',
      'id':1424160,
      'creation_date':'2017-06-19T09:19:41+0200',
      'update_date':'2018-07-13T10:00:47+0200',
      'slug':'oetker-collection-1424160',
      'name':'Oetker Collection',
      'type':'company'
    },
    'mentions':[

    ],
    'creation_date':'2018-08-10T17:57:00+0200',
    'thumbs':[
      {
        'path':'https://www.hosco.com/image/news-picture/1507657',
        'width':600,
        'height':600,
        'type':'image',
        'extension':'jpeg'
      }
    ],
    'files':[
      {
        'path':'https://cdn.hosco.com/assets/news/fb_9d36acc334014883cd3b35983899daa96adbab62.jpeg',
        'extension':'jpeg',
        'creation_date':'2018-08-11T04:56:05+0200',
        'width':2000,
        'height':2000,
        'type':'image'
      }
    ],
    'type':'picture'
  },
  {
    'comments':[

    ],
    'is_administrable':false,
    'is_mine':false,
    'is_liked':false,
    'share_count':0,
    'like_count':14,
    'comment_count':0,
    'url':'https://www.hosco.com/fr/newsfeed/status/885156',
    'story':'a partag\u00e9 un lien',
    'content':'What do you think of this new trend?\u00a0\ud83d\udcb0',
    'is_available':true,
    'id':885156,
    'owner':{
      'avatar':'https://www.hosco.com/image/logo/1044311/200/200',
      'id':670,
      'creation_date':'2011-08-23T21:23:45+0200',
      'update_date':'2018-05-22T09:36:26+0200',
      'slug':'hosco',
      'name':'Hosco',
      'type':'company'
    },
    'mentions':[

    ],
    'creation_date':'2018-08-10T16:03:20+0200',
    'link':{
      'host':'www.eturbonews.com',
      'id':67337,
      'title':'Hotels accepting money transfers to boost profits | Travel News | eTurboNews',
      'url':'https://www.eturbonews.com/230131/hotels-accepting-money-transfers-to-boost-profits',
      'picture':'https://www.eturbonews.com/wp-content/uploads/2018/08/0a1a1a1a-4.jpg',
      'description':'Hotels around the world, particularly in the UK and Ireland are now accepting international money transfers.',
      'date':'2018-08-09T00:00:00+0200',
      'creation_date':'2018-08-10T16:03:20+0200',
      'type':'website'
    },
    'type':'link'
  },
  {
    'comments':[

    ],
    'is_administrable':false,
    'is_mine':false,
    'is_liked':false,
    'share_count':0,
    'like_count':2,
    'comment_count':0,
    'url':'https://www.hosco.com/fr/newsfeed/status/885285',
    'story':'a partag\u00e9 un lien',
    'content':'From glamour and fine furnishings to multi-faceted expectations \ud83e\udd14... \nChris Cahill, our CEO, Luxury Brands, speaks in a Skift interview about the changes shaking the luxury industry and the group\u0027s challenges : to offer a sense of personal transformation \u0026 recognition. https://bit.ly/2OVQrAR',
    'is_available':true,
    'id':885285,
    'owner':{
      'avatar':'https://www.hosco.com/image/logo/883839/200/200',
      'id':968,
      'creation_date':'2012-10-31T17:19:48+0100',
      'update_date':'2018-06-29T11:32:18+0200',
      'slug':'accorhotels',
      'name':'AccorHotels',
      'type':'company'
    },
    'mentions':[

    ],
    'creation_date':'2018-08-10T11:47:08+0200',
    'link':{
      'host':'www.facebook.com',
      'id':67381,
      'title':'Chris Cahill - Driven by a Vision',
      'url':'https://www.facebook.com/AccorHotelsJobs/videos/10157891030204152/',
      'picture':'https://scontent.xx.fbcdn.net/v/t15.0-10/s160x160/37718799_10157891032674152_2411979837305520128_n.jpg?_nc_cat=0\u0026oh=20837f949713c200cde6763419d2660e\u0026oe=5C054174',
      'description':'From glamour and fine furnishings to multi-faceted expectations \ud83e\udd14... \nChris Cahill, our CEO, Luxury Brands, speaks in a Skift interview about the changes shaking the luxury industry and the group\u0027s challenges : to offer a sense of personal transformation \u0026 recognition. https://bit.ly/2OVQrAR',
      'creation_date':'2018-08-11T04:58:31+0200',
      'type':'website'
    },
    'type':'link'
  },
  {
    'comments':[

    ],
    'is_administrable':false,
    'is_mine':false,
    'is_liked':false,
    'share_count':0,
    'like_count':12,
    'comment_count':0,
    'url':'https://www.hosco.com/fr/newsfeed/status/885147',
    'story':'a partag\u00e9 un lien',
    'content':'Airbnb has already held events at unique or historic sites around the globe. \r\n.\r\nSo, should they have been allowed to have a sleepover on the Great Wall of China? \ud83e\udd14 \ud83c\udde8\ud83c\uddf3',
    'is_available':true,
    'id':885147,
    'owner':{
      'avatar':'https://www.hosco.com/image/logo/1044311/200/200',
      'id':670,
      'creation_date':'2011-08-23T21:23:45+0200',
      'update_date':'2018-05-22T09:36:26+0200',
      'slug':'hosco',
      'name':'Hosco',
      'type':'company'
    },
    'mentions':[

    ],
    'creation_date':'2018-08-10T12:52:07+0200',
    'link':{
      'host':'www.thedrum.com',
      'id':67325,
      'title':'Airbnb cancels Great Wall of China sleepover amid backlash from Beijing',
      'url':'https://www.thedrum.com/news/2018/08/08/airbnb-cancels-great-wall-china-sleepover-amid-backlash-beijing',
      'picture':'http://media-assets-01.thedrum.com/cache/images/thedrum-prod/s3-news-tmp-90538-untitled_design_1_3--default--1200.png',
      'description':'Airbnb has dropped plans to host a promotional sleepover on The Great Wall of China following a backlash from Beijing authorities.',
      'date':'2018-08-08T00:00:00+0200',
      'creation_date':'2018-08-10T12:48:04+0200',
      'type':'website'
    },
    'type':'link'
  },
  {
    'comments':[

    ],
    'is_administrable':false,
    'is_mine':false,
    'is_liked':false,
    'share_count':0,
    'like_count':0,
    'comment_count':0,
    'url':'https://www.hosco.com/fr/newsfeed/status/884975',
    'story':'a partag\u00e9 une photo',
    'content':'Perfect treats for the little ones @PalacioTangara \n#wherethemagichappens #kidsdream',
    'is_available':true,
    'id':884975,
    'owner':{
      'avatar':'https://www.hosco.com/image/logo/753068/200/200',
      'id':1424160,
      'creation_date':'2017-06-19T09:19:41+0200',
      'update_date':'2018-07-13T10:00:47+0200',
      'slug':'oetker-collection-1424160',
      'name':'Oetker Collection',
      'type':'company'
    },
    'mentions':[

    ],
    'creation_date':'2018-08-09T19:06:00+0200',
    'thumbs':[
      {
        'path':'https://www.hosco.com/image/news-picture/1504103',
        'width':600,
        'height':600,
        'type':'image',
        'extension':'jpeg'
      }
    ],
    'files':[
      {
        'path':'https://cdn.hosco.com/assets/news/fb_56b3de5b160be203873d1314569c580ab61efcfd.jpeg',
        'extension':'jpeg',
        'creation_date':'2018-08-10T04:56:54+0200',
        'width':2000,
        'height':2000,
        'type':'image'
      }
    ],
    'type':'picture'
  },
  {
    'comments':[

    ],
    'is_administrable':false,
    'is_mine':false,
    'is_liked':false,
    'share_count':0,
    'like_count':12,
    'comment_count':0,
    'url':'https://www.hosco.com/fr/newsfeed/status/884851',
    'story':'a partag\u00e9 un lien',
    'content':'Who has been to a Planet Hollywood restaurant or resort?\u00a0\r\n\r\nThe company has announced that it\u0027ll shortly be opening it\u0027s first ever all-inclusive resort in Costa Rica!\u00a0\u00a0\ud83c\udfa5\u00a0\ud83c\udf0e',
    'is_available':true,
    'id':884851,
    'owner':{
      'avatar':'https://www.hosco.com/image/logo/1044311/200/200',
      'id':670,
      'creation_date':'2011-08-23T21:23:45+0200',
      'update_date':'2018-05-22T09:36:26+0200',
      'slug':'hosco',
      'name':'Hosco',
      'type':'company'
    },
    'mentions':[

    ],
    'creation_date':'2018-08-09T17:50:47+0200',
    'link':{
      'host':'www.hospitalitynet.org',
      'id':67220,
      'title':'Iconic 90\u2019s Brand Planet Hollywood Ushers in New Era of Hospitality with First-Ever All-Inclusive Resort \u2013 Hospitality Net',
      'url':'https://www.hospitalitynet.org/announcement/41001381.html',
      'picture':'https://www.hospitalitynet.org/picture/xl_153091810.jpg',
      'description':'An iconic lifestyle brand born of the 1990\u0027s, Planet Hollywood is excited to announce its foray into the all-inclusive space with with its first resort, Planet Hollywood Beach Resort, slated to open October 1, 2018 on the Gulf of Papagayo in Costa Rica\u0027s Guanacaste province.',
      'creation_date':'2018-08-09T17:50:47+0200',
      'type':'website'
    },
    'type':'link'
  },
  {
    'comments':[

    ],
    'is_administrable':false,
    'is_mine':false,
    'is_liked':false,
    'share_count':0,
    'like_count':0,
    'comment_count':0,
    'url':'https://www.hosco.com/fr/newsfeed/status/885104',
    'story':'a partag\u00e9 une photo',
    'content':'D\u00e9cor du sud \ud83c\udf38\nL\u2019\u00e9clat du bougainvillier se m\u00eale \u00e0 la douceur \u00e9crue du bois flott\u00e9, pour \u00e9veiller vos sens et vous plonger dans l\u2019ambiance estivale du sud\u2026\n #TheWestinParisMoments #TheWestinParisTerrasse\n--\nA Southern decor \ud83c\udf38\nThe bougainvillea brightness mixed with the softness of driftwood will awaken your senses and draw you into the south summer mood.\n #TheWestinParisMoments #TheWestinParisTerrasse\n\n\u27a1\ufe0f https://west.tn/2Ko9m3M',
    'is_available':true,
    'id':885104,
    'owner':{
      'avatar':'https://www.hosco.com/image/logo/1743/200/200',
      'id':64753,
      'creation_date':'2016-01-19T11:39:05+0100',
      'update_date':'2018-07-23T12:41:14+0200',
      'slug':'westin-paris-vend\u00f4me',
      'name':'Westin Paris Vend\u00f4me',
      'type':'company'
    },
    'mentions':[

    ],
    'creation_date':'2018-08-09T14:00:00+0200',
    'thumbs':[
      {
        'path':'https://www.hosco.com/image/news-picture/1504206',
        'width':600,
        'height':898.902706656913,
        'type':'image',
        'extension':'jpeg'
      }
    ],
    'files':[
      {
        'path':'https://cdn.hosco.com/assets/news/fb_94e88bcaeffdc244d85fdc90fa604ea02018605f.jpeg',
        'extension':'jpeg',
        'creation_date':'2018-08-10T05:04:13+0200',
        'width':1367,
        'height':2048,
        'type':'image'
      }
    ],
    'type':'picture'
  },
  {
    'comments':[

    ],
    'is_administrable':false,
    'is_mine':false,
    'is_liked':false,
    'share_count':0,
    'like_count':1,
    'comment_count':0,
    'url':'https://www.hosco.com/fr/newsfeed/status/885036',
    'story':'a partag\u00e9 une photo',
    'content':'#Repost @passiontravelfoodie\n\u30fb\u30fb\u30fb\n#morning! So many choices in the #breakfast. What to start? \ud83e\udd57\ud83c\udf73\ud83c\udf5e\u2615\ufe0f\ud83c\udf79 #yum #foodie #brunch #goodvibes #hotelbreakfast #ootd #foodporn #switzerland #geneva \n#bonjour! Bcp de choix pour mon petit #d\u00e9jeuner dans \u27a1\ufe0fhotel kipling @manotelgroup  \u00e0 Gen\u00e8ve. #miam #bon #petitdejeuner /\n#\u745e\u58eb #\u65e5\u5167\u74e6\u3002\n#\u65e9\u5b89\uff0c #\u98ef\u5e97 #\u65e9\u9910\uff0c\n\u786c\u8981\u65e9\u8d77\uff0c\n\u4e0b\u6a13\u5230\u6211\u5bb6\u9694\u58c1\u98ef\u5e97\u5403\u65e9\u9910\u3002\n/\n\u5f9e\u65e9\u9910\u5403\u5230\u8b8a #\u65e9\u5348\u9910\u3002\n\u82e5\u662f\u98ef\u5e97\u4eba\u54e1\u6c92\u6709\u4f86\u8acb\u793a\u8acb\u96e2\u5e2d\uff0c\n\u53ef\u80fd\u6703\u8b8a #\u5348\u9910\uff01\ud83e\udd23\n/ \n#\u59d0\u59b9\u7d04\u6703 \u2764\ufe0f',
    'is_available':true,
    'id':885036,
    'owner':{
      'avatar':'https://www.hosco.com/image/logo/2607/200/200',
      'id':1610,
      'creation_date':'2014-02-17T12:27:21+0100',
      'update_date':'2018-05-09T14:29:41+0200',
      'slug':'manotel',
      'name':'Manotel Hotel Group Geneva',
      'type':'company'
    },
    'mentions':[

    ],
    'creation_date':'2018-08-09T12:35:46+0200',
    'thumbs':[
      {
        'path':'https://www.hosco.com/image/news-picture/1504157',
        'width':600,
        'height':600,
        'type':'image',
        'extension':'jpeg'
      }
    ],
    'files':[
      {
        'path':'https://cdn.hosco.com/assets/news/fb_3a02d3c10acfc093b63701365aa8a60a505e8bab.jpeg',
        'extension':'jpeg',
        'creation_date':'2018-08-10T05:00:26+0200',
        'width':1080,
        'height':1080,
        'type':'image'
      }
    ],
    'type':'picture'
  },
  {
    'comments':[
      {
        'content':'Agreed Docklands Academy London (DAL)!',
        'is_available':true,
        'id':7225,
        'owner':{
          'avatar':'https://www.hosco.com/image/logo/1134151/200/200',
          'id':8805,
          'creation_date':'2013-03-23T11:37:27+0100',
          'update_date':'2018-08-06T09:20:30+0200',
          'slug':'victoria-bitschnau',
          'location':{
            'address_display':'Barcelona, Espagne'
          },
          'name':'Victoria Bitschnau',
          'type':'member'
        },
        'mentions':[
          {
            'offset_start':7,
            'length':30,
            'profile':{
              'avatar':'https://www.hosco.com/image/logo/1308976/200/200',
              'id':2130566,
              'creation_date':'2018-06-12T10:27:44+0200',
              'update_date':'2018-08-04T13:53:21+0200',
              'slug':'docklands-academy-london-2130566',
              'name':'Docklands Academy London (DAL)',
              'type':'school'
            },
            'creation_date':'2018-08-09T16:32:02+0200'
          }
        ],
        'creation_date':'2018-08-09T16:32:02+0200'
      },
      {
        'content':'It\u0027s definitely a sector of the market that all businesses should consider. Family packages are a key tool in attracting a wide range of customers with specific needs and requirements.',
        'is_available':true,
        'id':7213,
        'owner':{
          'avatar':'https://www.hosco.com/image/logo/1308976/200/200',
          'id':2130566,
          'creation_date':'2018-06-12T10:27:44+0200',
          'update_date':'2018-08-04T13:53:21+0200',
          'slug':'docklands-academy-london-2130566',
          'name':'Docklands Academy London (DAL)',
          'type':'school'
        },
        'mentions':[

        ],
        'creation_date':'2018-08-09T11:40:50+0200'
      },
      {
        'content':'i interstated about this',
        'is_available':true,
        'id':7212,
        'owner':{
          'avatar':'https://www.hosco.com/image/logo/1501307/200/200',
          'id':2158321,
          'creation_date':'2018-06-27T10:57:41+0200',
          'update_date':'2018-08-09T10:14:37+0200',
          'slug':'md-shovo-khan',
          'location':{
            'address_display':'Mal\u00e9, Maldives'
          },
          'name':'Mohommad Shobo',
          'type':'member'
        },
        'mentions':[

        ],
        'creation_date':'2018-08-09T10:17:58+0200'
      }
    ],
    'is_administrable':false,
    'is_mine':false,
    'is_liked':false,
    'share_count':0,
    'like_count':13,
    'comment_count':3,
    'url':'https://www.hosco.com/fr/newsfeed/status/884822',
    'story':'a partag\u00e9 un lien',
    'content':'See how family travel is becoming\u00a0more significant for luxury hotels!\u00a0\ud83c\udfe8',
    'is_available':true,
    'id':884822,
    'owner':{
      'avatar':'https://www.hosco.com/image/logo/1044311/200/200',
      'id':670,
      'creation_date':'2011-08-23T21:23:45+0200',
      'update_date':'2018-05-22T09:36:26+0200',
      'slug':'hosco',
      'name':'Hosco',
      'type':'company'
    },
    'mentions':[

    ],
    'creation_date':'2018-08-09T10:07:11+0200',
    'link':{
      'host':'skift.com',
      'id':67208,
      'title':'Why Luxury Hotels Are Investing in Family-Friendly Programs',
      'url':'https://skift.com/2018/08/07/why-luxury-hotels-are-investing-in-family-friendly-programs/',
      'picture':'https://skift.com/wp-content/uploads/2018/07/BOS_934_aspect16x9-e1533219076142.jpg',
      'description':'Family travel is an enormous although occasionally overlooked segment of the luxury hospitality market \u2014 one that becomes all the more significant during Family travel is an enormous although occasionally overlooked segment of the luxury hospitality market',
      'date':'2018-08-07T00:00:00+0200',
      'creation_date':'2018-08-09T10:07:11+0200',
      'type':'website'
    },
    'type':'link'
  }
];
