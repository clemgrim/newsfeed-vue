import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    name: 'home',
    path: '/',
    component:  () => import('../pages/Home.vue' /* webpackChunkName: "home-page" */),
    meta: {
      noFooter: true,
    }
  },
  {
    name: 'news',
    path: '/news/:id',
    component:  () => import('../pages/News.vue' /* webpackChunkName: "news-page" */)
  },
  {
    name: '404',
    path: '*',
    component:  () => import('../pages/NotFound.vue' /* webpackChunkName: "not-found-page" */)
  },
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

export default router;
