import { storiesOf } from '@storybook/vue';

import { StoryPage, StoryContainer } from '~storybook/components';
import Widget from '../Base/WidgetBase.vue';
import WidgetListItem from './WidgetListItem.vue';

storiesOf('Widgets/List item', module)
  .add('Base list item', () => ({
    render() {
      return (
        <StoryPage component={WidgetListItem}>
          <StoryContainer size="320px">
            <WidgetListItem title="My title" link="#" info="Lorem ipsum dolor sit amet" picture="https://placehold.it/40x40" />
          </StoryContainer>
        </StoryPage>
      );
    }
  }))
  .add('Item with long text', () => ({
    render() {
      return (
        <StoryPage component={WidgetListItem}>
          <StoryContainer size="320px">
            <WidgetListItem
              title="My title lorem ipsum dolor sit amet lorem ipsum"
              link="#"
              info="Lorem ipsum dolor sit amet lorem ipsum dolor"
              picture="https://placehold.it/40x40"
            />
          </StoryContainer>
        </StoryPage>
      );
    }
  }))
  .add('Base with list items', () => ({
    render() {
      return (
        <StoryPage theme="dark" component={WidgetListItem}>
          <StoryContainer size="320px">
            <Widget title="My title" icon="fa fa-cogs">
              <WidgetListItem title="My title" link="#" info="Lorem ipsum dolor sit amet" picture="https://placehold.it/40x40" />
              <WidgetListItem title="My title II" link="#" info="Lorem ipsum dolor sit amet" picture="https://placehold.it/40x40" />
            </Widget>
          </StoryContainer>
        </StoryPage>
      );
    }
  }))
;
