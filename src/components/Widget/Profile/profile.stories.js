import { storiesOf } from '@storybook/vue';

import { StoryPage, StoryContainer } from '~storybook/components';
import ProfileWidget from './WidgetProfile.vue';

storiesOf('Widgets/Profile Widgets', module)
  .add('Profile widget', () => ({
    render() {
      return (
        <StoryPage theme="dark" component={ProfileWidget}>
          <StoryContainer size="320px">
            <ProfileWidget
              name="Clement Grimault"
              info="Lead developer at Hosco"
              link="#"
              avatar="https://www.hosco.com/image/logo/42332/40/40"
            />
          </StoryContainer>
        </StoryPage>
      );
    }
  }))
;
