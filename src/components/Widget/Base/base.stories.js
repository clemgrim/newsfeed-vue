import { storiesOf } from '@storybook/vue';

import { StoryPage, StoryContainer } from '~storybook/components';
import Widget from './WidgetBase.vue';

storiesOf('Widgets/Widgets', module)
  .add('Basic', () => ({
    render() {
      return (
        <StoryPage theme="dark" component={Widget} slots={['default']}>
          <StoryContainer size="320px">
            <Widget title="My title" icon="fa fa-cogs">This is my widget</Widget>
          </StoryContainer>
        </StoryPage>
      );
    }
  }))
  .add('Base without icon', () => ({
    render() {
      return (
        <StoryPage theme="dark" component={Widget} slots={['default']}>
          <StoryContainer size="320px">
            <Widget title="My title">This is my widget</Widget>
          </StoryContainer>
        </StoryPage>
      );
    }
  }))
  .add('Base without title', () => ({
    render() {
      return (
        <StoryPage theme="dark" component={Widget} slots={['default']}>
          <StoryContainer size="320px">
            <Widget>This is my widget</Widget>
          </StoryContainer>
        </StoryPage>
      );
    }
  }))
;
