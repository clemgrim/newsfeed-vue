import { shallowMount } from '@vue/test-utils';
import Avatar from './Avatar.vue';

describe('UI/Avatar', () => {
  it('src prop is set when passed', () => {
    const src = '://fake/img.jpg';
    const wrapper = shallowMount(Avatar, {
      propsData: { src }
    });
    const img = wrapper.find('img');

    expect(img.attributes().src).toBe(src);
  });

  it('matches snapshot', () => {
    const src = '://fake/img.jpg';
    const title = 'My title';
    const size = 'md';
    const rounded = true;
    const shadow = true;

    const wrapper = shallowMount(Avatar, {
      propsData: { src, title, size, rounded, shadow }
    });

    expect(wrapper.html()).toMatchSnapshot();
  });
});
