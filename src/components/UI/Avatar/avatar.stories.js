import { storiesOf } from '@storybook/vue';

import { StoryPage } from '~storybook/components';
import Avatar from './Avatar.vue';

const avatarSizes = ['xs', 'sm', 'md', 'lg'];

storiesOf('UI/Avatar', module)
  .add('Default', () => ({
    render() {
      return (
        <StoryPage component={Avatar}>
          {avatarSizes.map(size => (
            <p>
              <Avatar src="https://placehold.it/200x200" title="My profile" size={size} />
            </p>
          ))}
        </StoryPage>
      );
    }
  }))
  .add('Rounded', () => ({
    render() {
      return (
        <StoryPage component={Avatar}>
          {avatarSizes.map(size => (
            <p>
              <Avatar src="https://placehold.it/200x200" title="My profile" size={size} rounded={true} />
            </p>
          ))}
        </StoryPage>
      );
    }
  }))
  .add('Shadow', () => ({
    render() {
      return (
        <StoryPage component={Avatar}>
          {avatarSizes.map(size => (
            <p>
              <Avatar src="https://placehold.it/200x200" title="My profile" size={size} rounded={true} shadow={true} />
            </p>
          ))}
        </StoryPage>
      );
    }
  }))
;
