import { storiesOf } from '@storybook/vue';

import { StoryPage } from '~storybook/components';
import DateTime from './DateTime.vue';

const now = new Date().getTime();

storiesOf('UI/Date time', module)
  .add('Dates in the past', () => ({
    data() {
      return {
        dates: {
          now: now,
          fewSeconds: now - 5 * 1000,
          oneMinute: now - 60 * 1000,
          fewMinutes: now - 5 * 60 * 1000,
          oneHour: now - 60 * 60 * 1000,
          fewHours: now - 13 * 60 * 60 * 1000,
          oneDay: now - 25 * 60 * 60 * 1000,
          fewDays: now - 75 * 60 * 60 * 1000,
          fewMonths: now - 4 * 30 * 24 * 60 * 60 * 1000,
          oneYear: now - 365 * 24 * 60 * 60 * 1000,
          fewYears: now - 5 * 365 * 24 * 60 * 60 * 1000,
        }
      };
    },
    render() {
      return (
        <StoryPage component={DateTime}>
          {Object.entries(this.dates).map(([name, date]) =>
            <p>
              <strong>{name}</strong> <DateTime date={date} />
            </p>
          )}
        </StoryPage>
      );
    }
  }))
  .add('Dates in the future', () => ({
    data() {
      return {
        dates: {
          fewSeconds: now + 5 * 1000,
          oneMinute: now + 60 * 1000,
          fewMinutes: now + 5 * 60 * 1000,
          oneHour: now + 60 * 60 * 1000,
          fewHours: now + 13 * 60 * 60 * 1000,
          oneDay: now + 25 * 60 * 60 * 1000,
          fewDays: now + 75 * 60 * 60 * 1000,
          fewMonths: now + 4 * 30 * 24 * 60 * 60 * 1000,
          oneYear: now + 365 * 24 * 60 * 60 * 1000,
          fewYears: now + 5 * 365 * 24 * 60 * 60 * 1000,
        }
      };
    },
    render() {
      return (
        <StoryPage component={DateTime}>
          {Object.entries(this.dates).map(([name, date]) =>
            <p>
              <strong>{name}</strong> <DateTime date={date} />
            </p>
          )}
        </StoryPage>
      );
    }
  }))
  .add('Without time ago', () => ({
    data() {
      return {
        dates: {
          fewMinutesAgo: now - 5 * 60 * 1000,
          oneDayAgo: now - 25 * 60 * 60 * 1000,
          fewYearsAgo: now - 5 * 365 * 24 * 60 * 60 * 1000,
          inOneDay: now + 25 * 60 * 60 * 1000,
          inFewYears: now + 5 * 365 * 24 * 60 * 60 * 1000,
        }
      };
    },
    render() {
      return (
        <StoryPage component={DateTime}>
          {Object.entries(this.dates).map(([name, date]) =>
            <p>
              <strong>{name}</strong> <DateTime date={date} relative={false} />
            </p>
          )}
        </StoryPage>
      );
    }
  }))
  .add('Depending on the date', () => ({
    data() {
      return {
        dates: {
          fewMinutesAgo: now - 5 * 60 * 1000,
          fewDaysAgo: now - 75 * 60 * 60 * 1000,
          fewMonthsAgo: now - 2 * 30 * 24 * 60 * 60 * 1000,
          oneYearAgo: now - 365 * 24 * 60 * 60 * 1000,
          fewYearsAgo: now - 5 * 365 * 24 * 60 * 60 * 1000,
          inOneDay: now + 25 * 60 * 60 * 1000,
          inFewYears: now + 5 * 365 * 24 * 60 * 60 * 1000,
        },
        minDate: now - 6 * 30 * 24 * 60 * 60 * 1000,
        maxDate: now + 6 * 30 * 24 * 60 * 60 * 1000,
      };
    },
    render() {
      return (
        <StoryPage component={DateTime}>
          {Object.entries(this.dates).map(([name, date]) =>
            <p>
              <strong>{name}</strong> <DateTime date={date} relative={date > this.minDate && date < this.maxDate} />
            </p>
          )}
        </StoryPage>
      );
    }
  }))
;
