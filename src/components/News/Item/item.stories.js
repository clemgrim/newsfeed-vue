import { storiesOf } from '@storybook/vue';

import { StoryContainer, StoryPage } from '~storybook/components';
import NewsItem from './NewsItem.vue';

storiesOf('News/Item', module)
  .add('News', () => ({
    data() {
      return {
        news: {
          'comments':[

          ],
          'is_administrable':false,
          'is_mine':false,
          'is_liked':false,
          'share_count':0,
          'like_count':3,
          'comment_count':0,
          'url':'https://www.hosco.com/fr/newsfeed/status/885426',
          'story':'a partag\u00e9 une photo',
          'content':'WWW.CIPAS.INFO',
          'is_available':true,
          'id':885426,
          'owner':{
            'avatar':'https://www.hosco.com/image/logo/1290546/200/200',
            'id':241005,
            'creation_date':'2016-12-22T13:39:47+0100',
            'update_date':'2018-06-27T22:03:43+0200',
            'slug':'giancarlo-pastore',
            'location':{
              'address_display':'Pi\u00e9mont, Italie'
            },
            'name':'GIANCARLO PASTORE',
            'type':'member'
          },
          'mentions':[

          ],
          'creation_date':'2018-08-11T07:44:28+0200',
          'update_date':'2018-08-11T07:44:40+0200',
          'thumbs':[
            {
              'path':'https://www.hosco.com/image/news-picture/1507978',
              'width':600,
              'height':425.9354177344951,
              'type':'image',
              'extension':'jpeg'
            }
          ],
          'files':[
            {
              'path':'https://cdn.hosco.com/upload/7c5e751870ed9f7e89a1cadc2896d8fdb8152bc05b6e77bc4a1370.24432144.jpeg',
              'title':'ON LINE MASTER HOTEL \u0026 REVENUE MANAGEMENT.jpeg',
              'extension':'jpeg',
              'creation_date':'2018-08-11T07:44:28+0200',
              'width':1951,
              'height':1385,
              'type':'image'
            }
          ],
          'type':'picture'
        }
      };
    },
    render() {
      return (
        <StoryPage theme="dark" component={NewsItem}>
          <StoryContainer size="610px">
            <NewsItem news={this.news} />
          </StoryContainer>
        </StoryPage>
      );
    }
  }))
;
