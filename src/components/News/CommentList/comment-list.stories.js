import { storiesOf } from '@storybook/vue';

import { StoryContainer, StoryPage } from '~storybook/components';
import CommentList from './CommentList.vue';

storiesOf('News/Comment List', module)
  .add('Default', () => ({
    data() {
      return {
        comments: [
          {
            'content':'Agreed Docklands Academy London (DAL)!',
            'is_available':true,
            'id':7225,
            'owner':{
              'avatar':'https://www.hosco.com/image/logo/1134151/200/200',
              'id':8805,
              'creation_date':'2013-03-23T11:37:27+0100',
              'update_date':'2018-08-06T09:20:30+0200',
              'slug':'victoria-bitschnau',
              'location':{
                'address_display':'Barcelona, Espagne'
              },
              'name':'Victoria Bitschnau',
              'type':'member'
            },
            'mentions':[
              {
                'offset_start':7,
                'length':30,
                'profile':{
                  'avatar':'https://www.hosco.com/image/logo/1308976/200/200',
                  'id':2130566,
                  'creation_date':'2018-06-12T10:27:44+0200',
                  'update_date':'2018-08-04T13:53:21+0200',
                  'slug':'docklands-academy-london-2130566',
                  'name':'Docklands Academy London (DAL)',
                  'type':'school'
                },
                'creation_date':'2018-08-09T16:32:02+0200'
              }
            ],
            'creation_date':'2018-08-09T16:32:02+0200'
          },
          {
            'content':'It\u0027s definitely a sector of the market that all businesses should consider. Family packages are a key tool in attracting a wide range of customers with specific needs and requirements.',
            'is_available':true,
            'id':7213,
            'owner':{
              'avatar':'https://www.hosco.com/image/logo/1308976/200/200',
              'id':2130566,
              'creation_date':'2018-06-12T10:27:44+0200',
              'update_date':'2018-08-04T13:53:21+0200',
              'slug':'docklands-academy-london-2130566',
              'name':'Docklands Academy London (DAL)',
              'type':'school'
            },
            'mentions':[

            ],
            'creation_date':'2018-08-09T11:40:50+0200'
          },
          {
            'content':'i interstated about this',
            'is_available':true,
            'id':7212,
            'owner':{
              'avatar':'https://www.hosco.com/image/logo/1501307/200/200',
              'id':2158321,
              'creation_date':'2018-06-27T10:57:41+0200',
              'update_date':'2018-08-09T10:14:37+0200',
              'slug':'md-shovo-khan',
              'location':{
                'address_display':'Mal\u00e9, Maldives'
              },
              'name':'Mohommad Shobo',
              'type':'member'
            },
            'mentions':[

            ],
            'creation_date':'2018-08-09T10:17:58+0200'
          }
        ]
      };
    },
    render() {
      return (
        <StoryPage theme="light" component={CommentList}>
          <StoryContainer size="610px">
            <CommentList comments={this.comments} comments-count={5} />
          </StoryContainer>
        </StoryPage>
      );
    }
  }))
;
