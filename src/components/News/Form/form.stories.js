import { storiesOf } from '@storybook/vue';

import { StoryContainer, StoryPage } from '~storybook/components';
import NewsForm from './NewsForm.vue';

storiesOf('News/Form', module)
  .add('News form', () => ({
    render() {
      return (
        <StoryPage theme="dark" component={NewsForm}>
          <StoryContainer size="610px">
            <NewsForm  avatar="https://www.hosco.com/image/logo/42332/40/40" />
          </StoryContainer>
        </StoryPage>
      );
    }
  }))
;
