import { storiesOf } from '@storybook/vue';

import { StoryPage } from '~storybook/components';
import TwoColsLayout from './TwoCols.vue';

storiesOf('Layout', module)
  .add('Two columns layout', () => ({
    render() {
      return (
        <StoryPage component={TwoColsLayout} slots={['content', 'sidebar']}>
          <TwoColsLayout>
            <div slot="content">
              <h1>This is the content</h1>
              <p>Lorem ipsum dolor sit amet....</p>
            </div>
            <div slot="sidebar">
              This is the sidebar
            </div>
          </TwoColsLayout>
        </StoryPage>
      )
    }
  }))
;
