import { storiesOf } from '@storybook/vue';

import { StoryPage } from '~storybook/components';
import AppFooter from './AppFooter.vue';

storiesOf('Layout', module)
  .add('Footer', () => ({
    render() {
      return (
        <StoryPage component={AppFooter}>
          <AppFooter />
        </StoryPage>
      );
    }
  }))
;
