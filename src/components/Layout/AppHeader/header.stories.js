import { storiesOf } from '@storybook/vue';

import { StoryPage } from '~storybook/components';
import AppHeader from './AppHeader.vue';
import AppHeaderIcon from './AppHeaderIcon.vue';

storiesOf('Layout', module)
  .add('Header', () => ({
    render() {
      return (
        <StoryPage component={AppHeader}>
          <AppHeader fixed={false} />
        </StoryPage>
      );
    }
  }))
  .add('Header icon', () => ({
    render() {
      return (
        <StoryPage component={AppHeaderIcon}>
          <b-navbar toggleable="md" type="light" variant="white">
            <b-navbar-nav class="ml-auto">
              <b-nav-item>
                <AppHeaderIcon icon="fa fa-cogs"/>
              </b-nav-item>
              <b-nav-item>
                <AppHeaderIcon icon="fa fa-user-plus" count={4} />
              </b-nav-item>
              <b-nav-item>
                <AppHeaderIcon icon="fa fa-bell-o" count={99} />
              </b-nav-item>
              <b-nav-item>
                <AppHeaderIcon icon="fa fa-envelope-o" count={585} />
              </b-nav-item>
              <b-nav-item>
                <AppHeaderIcon icon="fa fa-share-alt" count={5568} />
              </b-nav-item>
            </b-navbar-nav>
          </b-navbar>
        </StoryPage>
      );
    }
  }))
;
