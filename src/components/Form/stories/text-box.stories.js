import { storiesOf } from '@storybook/vue';

import { StoryPage } from '~storybook/components';
import TextBox from '../TextBox.vue';

storiesOf('Form/Text box', module)
  .add('Text input', () => ({
    render() {
      return (
        <StoryPage component={TextBox}>
          <TextBox type="text" placeholder="Lorem ipsum" />
        </StoryPage>
      );
    }
  }))
  .add('Textarea', () => ({
    render() {
      return (
        <StoryPage component={TextBox}>
          <TextBox type="textarea" placeholder="Lorem ipsum" />
        </StoryPage>
      );
    }
  }))
;
