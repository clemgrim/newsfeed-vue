import { storiesOf } from '@storybook/vue';

import { StoryPage } from '~storybook/components';
import SelectBox from '../SelectBox.vue';

const suggestions = [
  {flag: '🇫🇷', code: 'FR', name: 'Francía'},
  {flag: '🇧🇪', code: 'BE', name: 'Belgica'},
  {flag: '🇪🇸', code: 'ES', name: 'Esnaña'},
  {flag: '🇨🇭', code: 'CH', name: 'Suiza'},
  {flag: '🇩🇪', code: 'DE', name: 'Alemania'},
  {flag: '🇮🇹', code: 'IT', name: 'Italia'},
  {flag: '🇬🇧', code: 'UK', name: 'United Kingdom'},
  {flag: '🇮🇹', code: 'US', name: 'United States'},
  {flag: '🇨🇳', code: 'CN', name: 'China'},
  {flag: '🇵🇹', code: 'PT', name: 'Portugal'},
  {flag: '🇷🇺', code: 'RU', name: 'Russia'},
  {flag: '🇯🇵', code: 'JA', name: 'Japón'},
  {flag: '🇳🇱', code: 'NL', name: 'Netherlands'},
  {flag: '🇸🇪', code: 'SW', name: 'Sweden'},
  {flag: '🇳🇴', code: 'NW', name: 'Norway'},
  {flag: '🇫🇮', code: 'FN', name: 'Finland'},
  {flag: '🇩🇰', code: 'DN', name: 'Denmark'},
];

storiesOf('Form/Select box', module)
  .add('Array of string', () => ({
    render() {
      return (
        <StoryPage component={SelectBox}>
          <SelectBox
            placeholder="Lorem ipsum"
            suggestions={['Approved', 'Rejected', 'Pending']}
            minLength={0}
          />
        </StoryPage>
      );
    }
  }))
  .add('Array of objects with custom renderer', () => ({
    render() {
      return (
        <StoryPage component={SelectBox}>
          <SelectBox
            placeholder="Lorem ipsum"
            suggestions={suggestions}
            scopedSlots={{ default: ({ item }) => item.flag + ' ' + item.name }}
          >
          </SelectBox>
        </StoryPage>
      );
    }
  }))
;
