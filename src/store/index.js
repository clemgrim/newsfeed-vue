import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import jobs from './modules/jobs';
import identity from './modules/identity';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Store({
  modules: {
    jobs,
    identity,
  },
  strict: debug,
});
