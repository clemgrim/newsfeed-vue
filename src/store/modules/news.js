const state = {
  news: {
    isLoading: false,
    error: null,
    result: null
  },
};

const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
const FETCH_NEWS_FAILURE = 'FETCH_NEWS_FAILURE';

const TOGGLE_LIKE_REQUEST = 'TOGGLE_LIKE_REQUEST';
const TOGGLE_LIKE_SUCCESS = 'TOGGLE_LIKE_SUCCESS';

const SHARE_NEWS_REQUEST = 'SHARE_NEWS_REQUEST';
const SHARE_NEWS_SUCCESS = 'SHARE_NEWS_SUCCESS';

const findNews = async (id) => {
  const result = await import('../../data/newsfeed');
  const news = result.default.find(o => o.id === id);

  if (!news) {
    throw new Error('News not found');
  }

  return news;
};

const wait = (delay) => new Promise(resolve => setTimeout(resolve, delay));

const actions = {
  async fetchNews({ commit }, id) {
    commit(FETCH_NEWS_REQUEST);

    try {
      const result = await findNews(id);
      commit(FETCH_NEWS_SUCCESS, result);
    } catch (e) {
      commit(FETCH_NEWS_FAILURE, e);
    }
  },
  async toggleLike({ commit }) {
    commit(TOGGLE_LIKE_REQUEST);

    await wait(1500);

    commit(TOGGLE_LIKE_SUCCESS);
  },
  async shareNews({ commit }) {
    commit(SHARE_NEWS_REQUEST);

    await wait(1500);

    commit(SHARE_NEWS_SUCCESS);
  },
};

const mutations = {
  [FETCH_NEWS_SUCCESS](state, result) {
    state.news.result = { ...result, likeLoading: false, shareLoading: false };
    state.news.error = null;
    state.news.isLoading = false;
  },
  [FETCH_NEWS_REQUEST](state) {
    state.news.isLoading = true;
  },
  [FETCH_NEWS_FAILURE](state, e) {
    state.news.error = e;
    state.news.isLoading = false;
  },
  [TOGGLE_LIKE_SUCCESS](state) {
    const news = state.news.result;

    news.likeLoading = false;
    news.is_liked = !news.is_liked;
    news.like_count += news.is_liked ? 1 : -1;
  },
  [TOGGLE_LIKE_REQUEST](state) {
    const news = state.news.result;
    news.likeLoading = true;
  },
  [SHARE_NEWS_SUCCESS](state) {
    const news = state.news.result;
    news.share_count++;
    news.shareLoading = false;
  },
  [SHARE_NEWS_REQUEST](state) {
    const news = state.news.result;
    news.shareLoading = true;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
