const state = {
  profile: {
    name: 'Clement Grimault',
    title: 'Lead developer at Hosco',
    url: '#',
    avatar: 'https://www.hosco.com/image/logo/42332/40/40'
  }
};

export default {
  namespaced: true,
  state,
};
