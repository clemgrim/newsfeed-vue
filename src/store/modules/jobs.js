const FETCH_JOB_REQUEST = 'FETCH_JOB_REQUEST';
const FETCH_JOB_FAILURE = 'FETCH_JOB_FAILURE';
const FETCH_JOB_SUCCESS = 'FETCH_JOB_SUCCESS';

const state = {
  suggestedJobs: {
    loading: false,
    error: null,
    items: []
  }
};

const actions = {
  async loadSuggestedJobs({ commit }) {
    commit(FETCH_JOB_REQUEST);

    try {
      const result = await import('../../data/suggested-jobs');
      commit(FETCH_JOB_SUCCESS, result.default);

      return result;
    } catch (e) {
      commit(FETCH_JOB_FAILURE, e);

      return Promise.reject(e);
    }
  }
};

const mutations = {
  [FETCH_JOB_REQUEST]({ suggestedJobs }) {
    suggestedJobs.loading = true;
  },
  [FETCH_JOB_SUCCESS]({ suggestedJobs }, result) {
    suggestedJobs.items = result;
    suggestedJobs.error = null;
    suggestedJobs.loading = false;
  },
  [FETCH_JOB_FAILURE]({ suggestedJobs }, e) {
    suggestedJobs.error = e;
    suggestedJobs.loading = false;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
