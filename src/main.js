import Vue from 'vue';
import VueMeta from 'vue-meta';
import router from './router';
import store from './store';
import App from './App.vue';

Vue.use(VueMeta);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app');
